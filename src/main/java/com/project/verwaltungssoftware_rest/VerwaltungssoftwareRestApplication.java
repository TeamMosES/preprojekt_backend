package com.project.verwaltungssoftware_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VerwaltungssoftwareRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(VerwaltungssoftwareRestApplication.class, args);
    }

}
